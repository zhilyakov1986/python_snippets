def selSort(L):
    '''Assumes that L is a list of elements that can be compared
    using >.
    Sorts L in ascending order
    Arguments:
        L {[list]} -- [sortable list]
    '''
    suffixStart = 0
    while suffixStart != len(L):
        # Look at each element in the suffix
        for i in range(suffixStart, len(L)):
            if L[i] < L[suffixStart]:
                # swap position of elements
                L[suffixStart], L[i] = L[i], L[suffixStart]
        suffixStart += 1


print(selSort([9, 2, 4, 5, 6, 7, 1]))
