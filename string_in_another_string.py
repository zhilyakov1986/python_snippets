def isIn(word_one, word_two):
    '''finds out if one string is the part of anther

    Arguments:
        word_one {[strting]} -- [stting of text]
        word_two {[strting]} -- [stting of text]

    Returns:
        [bool] -- [if one string is the part of another]
    '''

    if word_one in word_two:
        return True
    if word_two in word_one:
        return True
    return False


print(isIn(word_one='abc', word_two='bc'))
