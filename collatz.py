def collatz(number):
    if number % 2 == 0:
        print(number//2)
        return number//2
    else:
        print(3*number+1)
        return 3*number+1

try:
    result = int(input('Enter number: '))
    while result != 1:
        result = collatz(result)
except ValueError:
    print('Value has to be a digit')

