import functools
import string
import math


def volume_sphere(rad):
    '''find volume of the sphere given a
    radius
    V = 4/3 πr³
    Arguments:
        rad {float} -- given radius
    '''
    return 4/3*(math.pi*rad**3)

# print(volume_sphere(5))


def ran_check(num, low, high):
    '''checks wether e num in the current range

    Arguments:
        num {float} -- number we are searching for
        low {float} -- lower boundary
        high {float} -- upper boundary
    '''
    if num in range(low, high+1):
        return num


def ran_bool_check(num, low, high):
    if num in range(low, high+1):
        return True
    return False


# print(ran_bool_check(2, 1, 6))


def calculate_letters(sentance):
    '''takes a string and outputs the number of upper
    and number of lower case letters

    Arguments:
        sentence {[str]} -- [sentance with variously cased letters]
    '''
    lower_case = string.ascii_lowercase
    upper_case = string.ascii_uppercase

    lower_counter = 0
    upper_counter = 0
    for letter in sentance:
        if letter in lower_case:
            lower_counter += 1
        if letter in upper_case:
            upper_counter += 1
    print(f'there are {lower_counter} lower case letters in the string')
    print(f'\nand there are {upper_counter} upper case letters in the string')

# calculate_letters('adfasdfdfadfaADFAADFA')


# Write a function to multiply all numbers in the list
print(functools.reduce(lambda a, b: a*b, [1, 2, 3, -4]))

# Write a function to detect if the string is palindrome read the same from
# beginning and from the end


def is_palindrome(str):
    all_letters = string.ascii_letters
    new_str_list = [letter for letter in str if letter in all_letters]
    reversed_str_list = new_str_list
    reversed_str_list.reverse()
    if ''.join(new_str_list) == ''.join(reversed_str_list):
        return True
    else:
        return False

# print(is_palindrome('madam'))   --True
#print(is_palindrome('nurses run')) --True
