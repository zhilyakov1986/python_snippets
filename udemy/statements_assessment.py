
# # prints only words that start from s from the string st
# st = 'Print only the words that start with s in this sentence'
# list_st = [word for word in st.split() if word[0] == 's']
# print(list_st)

# # use range() to print all even numbers from 0 to 10
# for i in range(0, 11, 2):
#     print(i)
# # second way
# print(list(range(0, 11, 2)))

# # use list comprehension to create the list of numbers from 0 to 50 divisible by 3
# num_list = [num for num in range(0, 51) if num % 3 == 0]
# print(num_list)


# # Go through the string below and if the length of a word is even print "even!"
# st = 'Print every word in this sentence that has an even number of letters'
# list_st = st.split(st)
# for word in list_st:
#     if len(word) % 2 == 0:
#         print('even')

# # Write a program that prints the integers from 1 to 100. But for multiples of three
# # print "Fizz" instead of the number, and for the multiples of five print "Buzz".
# # For numbers which are multiples of both three and five print "FizzBuzz".
# for i in range(1, 101):
#     if i % 3 == 0 and i % 5 == 0:
#         print('FizzBuzz')
#     elif i % 5 == 0:
#         print('Buzz')
#     elif i % 3 == 0:
#         print('Fizz')
#     else:
#         print(i)
# # Use List Comprehension to create a list of the first letters of every word in the string below:
# st = 'Create a list of the first letters of every word in this string'
# first_three = [word[0:3] for word in st.split()]
# print(first_three)

# using enumerate function
import string
import math


def myfunc(normal_string) -> list:
    name_list = list(enumerate(normal_string))
    mixed_case_list = []
    for enum, letter in name_list:
        if enum % 2 != 0:
            mixed_case_list.append(letter.upper())
        if enum % 2 == 0:
            mixed_case_list.append(letter.lower())
    return ''.join(mixed_case_list)


def lesser_of_two_evens(a, b):
    '''
    returns the lesser of two given numbers if both numbers are even, 
    but returns the greater if one or both numbers are odd
    '''
    if a % 2 == 0 and b % 2 == 0:
        return min(a, b)
    else:
        return max(a, b)

# TODO
# def lesser_of_two_evens_ternary(a, b):
#     # x if condition else y
#     return min(a, b) if (a % 2 == 0 and b % 2 == 0) else return max(a, b)


# print(lesser_of_two_evens_ternary(2,5))

def animal_crackers(text):
    ''' 
    Write a function takes a two-word string and returns 
    True if both words begin with same letter
    '''
    return text.split()[0][0].lower == text.split()[1][0].lower


def makes_twenty(n1, n2):
    '''
    Given two integers, return True if the sum of the integers is
    20 or if one of the integers is 20. If not, return False

    '''
    return n1 + n2 == 20 or n1 == 20 or n2 == 20


def master_yoda(name):
    '''
    Given a sentence, return a sentence with the words reversed
    '''
    name_list = name.split()
    name_list.reverse()
    return ' '.join(name_list)


# print(master_yoda('I am home'))


def old_macdonald(name):
    '''
    Write a function that capitalizes the first and fourth letters of a name
    '''
    letters_list = list(name)
    letters_list[0] = letters_list[0].upper()
    letters_list[3] = letters_list[3].upper()
    return ''.join(letters_list)

# print(old_macdonald('macdonald'))


def almost_there(n):
    '''
    Given an integer n, return True if n is within 10 of either 100 or 200
    '''
    return n in range(90, 111) or n in range(190, 211)


# print(almost_there(150))

def has_33(nums):
    '''Given a list of ints, return True if the array contains a 3 next to a 3 somewhere.'''
    str_nums = [str(n) for n in nums]
    string_nums = ''.join(str_nums)
    print(string_nums)
    return '33' in string_nums


def has_33_using_slices(nums):
    for i in range(0, len(nums)-1):
        # remember that upper range of slice doesn't include the last number
        # hence we have i+2 instead of i+1 !!!
        if nums[i:i+2] == [3, 3]:
            return True
    return False


print(has_33_using_slices([1, 3, 3, 1]))


def paper_doll(text):
    '''
    Given a string, return a string where for every 
    character in the original there are three characters
    '''
    return ''.join([letter*3 for letter in text])


# print(paper_doll('Hello'))


def blackjack(a, b, c):
    '''
    Given three integers between 1 and 11, if their sum is less than or equal to 21, 
    return their sum. If their sum exceeds 21 and there's an eleven, 
    reduce the total sum by 10. Finally, if the sum (even after adjustment) exceeds 21,
     return 'BUST'
    '''
    if sum([a, b, c]) <= 21:
        return sum([a, b, c])
    if sum([a, b, c]) > 21 and max(a, b, c) == 11:
        return sum([a, b, c]) - 10
    return "BUST"


# print(blackjack(9, 9, 11))

def summer_69(arr):
    '''
    Return the sum of the numbers in the array, except ignore sections of 
    numbers starting with a 6 and extending to the next 9 
    (every 6 will be followed by at least one 9). Return 0 for no numbers.
    '''
    flag = True
    sum = 0
    for num in arr:
        if num == 6:
            flag = False
            continue
        if num == 9:
            flag = True
            continue
        if flag:
            sum += num
        if not flag:
            continue
    return sum


# print(summer_69([2, 1, 6, 9, 11]))

def spy_game(nums):
    '''Write a function that takes in a list of integers and returns True if it contains 007 in order'''
    short_list = [str(i) for i in nums if i == 0 or i == 7]
    short_str = ''.join(short_list)
    return '007' in short_str


# def is_prime(n):
#     if n % 2 == 0 and n > 2:
#         return False
#     for i in range(3, int(math.sqrt(n)) + 1, 2):
#         if n % i == 0:
#             return False
#     return True


# Note that this code does not properly handle 0, 1, and negative numbers.

# We make this simpler by using all with a generator expression to replace the for-loop.


# def is_prime(n):
#     if n % 2 == 0 and n > 2:
#         return False
#     # print(spy_game([1, 7, 2, 0, 4, 5, 0]))
#     return all(n % i for i in range(3, int(math.sqrt(n)) + 1, 2))


def is_prime(n):
    # in n is even return false
    if n % 2 == 0 and n > 2:
        return False

    for i in range(3, int(math.sqrt(n)+1), 2):
        if n % i == 0:
            return False
    return True


def count_primes(num):
    '''
    Write a function that returns the number of prime numbers 
    that exist up to and including a given number
    '''
    counter = 0
    for i in range(2, num+1):
        if is_prime(i):
            counter += 1
    return counter


print(count_primes(100))


def is_pangram(str1, alphabeth=string.ascii_letters):
    for letter in alphabeth:
        if letter not in str1:
            return False
    return True
