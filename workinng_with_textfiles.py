# to read the file
with open('my_new_file.txt', mode='r') as f:
    print(f.read())

# to append the file
# with open('my_new_file.txt', mode='a') as f:
#     print(f.write('\nFOUR ON FORTH'))

# to write the file it will overwrte the existing file or create new one if the
# file doesn't exist already
with open('my_new_file_name.txt', mode='w') as f:
    f.write('I created this file!')
