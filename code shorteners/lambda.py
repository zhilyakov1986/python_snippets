# using filter function
# lambda function used as pridicate, returns true or false
print(list(filter(lambda num: num % 2 == 0, [1, 2, 3, 4, 5, 6])))

# using map function
# lambda has to take one argument of type of the iterable that being passed along with it
print(list(map(lambda n: n**2, [1, 2, 3, 4, 5])))
