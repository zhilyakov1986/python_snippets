# def say_hello():
#     print('Hello')

# for i in range(5):
#     say_hello()

# question 1: write a function that reverses a string


def reverse_string(original_string):
    return original_string[::-1]


def reverse_string_manual(original_string):
    # n - complexity
    output = ''
    for i in original_string:
        output = i + output
    return output


def reverse_string_array(original_string):
    output_len = len(original_string)
    output = [None]*output_len
    output_index = output_len-1
    for char in original_string:
        output[output_index] = char
        output_index -= 1
    return ''.join(output)


def find_missing(list_one, list_two):
    # two unique lists, find the element that is missing from list_two
    # that is in the list one
    # ex list_one[4,12,9,5,6] and list_two[4,9,12,6] should return 5
    output = []
    longer_list = []
    smaller_list = []
    if len(list_one) >= len(list_two):
        longer_list = list_one
        smaller_list = list_two
    else:
        longer_list = list_two
        smaller_list = list_one
    for i in longer_list:
        if i not in smaller_list:
            output.append(i)
    return output


def find_missing_pythonic(full_set, partial_set):
    missing_items = set(full_set) - set(partial_set)
    assert(len(missing_items) == 1)
    return missing_items.pop()


def find_missing_xor(full_set, partial_set):
    xor_sum = 0
    for num in full_set:
        xor_sum ^= num
    for num in partial_set:
        xor_sum ^= num

    return xor_sum


print(find_missing_xor([4, 12, 9, 6, 5], [4, 9, 12, 6]))
