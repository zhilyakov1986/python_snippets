# # Exaustive enumeration
# user_input = int(input('Please enter an integer: '))
# ans = 0
# while ans**3 < abs(user_input):
#     ans += 1
# if ans**3 != user_input:
#     print(f'{user_input} is not a perfect cube')
# else:
#     if user_input < 0:
#         ans = -ans
#     print(f'{user_input} is a perfect cube')
# ------------------------------------------------------------------------------
# user_input = int(input('Please enter an integer: '))
# root = 0
# pow = 1
# while pow < 6:
#     while abs(root**pow) <= abs(user_input):
#         root += 1
#         if root**pow == user_input:
#             print(f'{root} is a perfect root and {pow} is the power')
#     else:
#         print(f'{root} is a perfect root and {pow} is the power')
#         break
#     pow += 1
# print(f'not pair that of int exist for {user_input}')
# ------------------------------------------------------------------------------
# user_input = int(input('enter an int: '))
# root = 0
# pow = 1
# while pow < 6:
#     root = 0
#     while abs(root**pow) < abs(user_input):
#         root += 1
#         if root**pow == user_input:
#             print(f'{pow} {root}')
#             break
#     pow += 1
# ------------------------------------------------------------------------------
# total = 0
# nums = '1.23,2.4,3.123'
# list_of_nums = nums.split(',')
# for c in list_of_nums:
#     total += float(c)
# print(total)
# ------------------------------------------------------------------------------
# find the square root with presicion of epsilon
# epsilon = 0.01
# user_input = int(
#     input('enter the value you wish to find the square root off: '))
# ans = 0
# number_of_guesses =
# step = epsilon**2
# while abs(ans**2-user_input) >= epsilon:
#     number_of_guesses += 1
#     ans += step
# print(f'Number of guesses is {number_of_guesses}')
# if abs(ans**2-user_input) >= epsilon:
#     print(f'failed on squere root of {user_input}')
# else:
#     print(f'{ans: .2f} is close to square root of {user_input}')
# ------------------------------------------------------------------------------
# user_input = int(
#     input('enter the value you wish to find the square root off: '))
# user_input_abs = abs(user_input)
# low = 0.0
# high = max(1.0, user_input_abs)
# ans = (low+high)/2.0
# number_of_guesses = 0
# epsilon = 0.01
# while abs(ans**2-user_input_abs) >= epsilon:
#     #print(f'low = {low} high = {high} ans={ans}')
#     number_of_guesses += 1
#     if ans**2 < user_input_abs:
#         low = ans
#     else:
#         high = ans
#     ans = (low + high)/2
# print(f'nubmer of guesses: {number_of_guesses}')
# if user_input < 0:
#     ans = -ans
# print(f'{ans} is clos to square root of {user_input}')
# ------------------------------------------------------------------------------
# user_input = int(
#     input('enter the value you wish to find the cube root off: '))
# user_input_abs = abs(user_input)
# low = 0.0
# high = max(1.0, user_input_abs)
# ans = (low+high)/2.0
# number_of_guesses = 0
# epsilon = 0.01
# while abs(ans**3-user_input_abs) >= epsilon:
#     #print(f'low = {low} high = {high} ans={ans}')
#     number_of_guesses += 1
#     if ans**3 < user_input_abs:
#         low = ans
#     else:
#         high = ans
#     ans = (low + high)/2
# print(f'nubmer of guesses: {number_of_guesses}')
# if user_input < 0:
#     ans = -ans
# print(f'{ans} is close to square root of {user_input}')
# ------------------------------------------------------------------------------
# print(bin(19))
# print(int('11', 2))

# Newton-Raphson for square root of 24
# we create a polindrome x**2-24 if we need a quadratic root
# for cube root we would use x**3-24
# -------------------------------------------------------------------------------
# Find x such that x**2-24 is within epsilon of 0
# epsilon = 0.01
# k = 24.0
# guess = k/2.0
# number_of_guesses = 0
# while abs(guess*guess-k) >= epsilon:
#     number_of_guesses += number_of_guesses
#     guess = guess - (((guess**2)-k)/(2*guess))
# print(f'number of guesses {number_of_guesses}')
# print(f'Square root of {k} is about {guess}')
