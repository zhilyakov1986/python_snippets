
# class Solution:
#     def twoSum(self, nums: list[int], target: int) ->list[int]:
#         '''
#         returns the list of indices of two numbers that sums to target 
#         '''
#         prefix = 0
#         while prefix != len(nums):
#             #see if the sum of num[prefix] with
#             #any of the rest of the numbers results
#             #in target number
#             for i in range(prefix+1, len(nums)):
#                 if nums[prefix]+ nums[i] == target:
#                     return [nums[prefix],nums[i]]
#             #in it is not then move prefix one digit to the right 
#             #and try again
#             prefix+=1
            