def create_cubes(n):
    for i in range(n):
        yield i**3

# print(list(create_cubes(10)))


def gen_fib(n):
    a = 1
    b = 1
    for i in range(n):
        yield a
        a, b = b, a+b

# print(list(gen_fib(10)))
