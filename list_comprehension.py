# regular way
mystring = 'hello'
mylist = []
for letter in mystring:
    mylist.append(letter)
print(mylist)

# casting string to list
newList = list(mystring)
print(newList)

# list comprehension
listComprehensionString = [l for l in mystring]
print(listComprehensionString)

# we can perform number operations in list comprehension
numList = [num**2 for num in range(1, 11)]
print(numList)

# we can add if statements to it
numListIF = [num for num in range(1, 11) if num % 2 == 0]
print(numListIF)

# we can also use IF ELSE
numListIFELSE = [x if x % 2 == 0 else 'ODD' for x in range(0, 11)]
print(numListIFELSE)
################################
############  NESTED LISTS #####
################################

# regular way
nestRegList = []
for x in [2, 4, 6]:
    for y in [100, 200, 300]:
        nestRegList.append(x*y)
print(nestRegList)

# list comprehension way
listCompNested = [x*y for x in [2, 4, 6] for y in [100, 200, 300]]
print(listCompNested)
